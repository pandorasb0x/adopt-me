import { configureStore } from "@reduxjs/toolkit";
import adoptedPet from "./AdoptedPetSlice";

const store = configureStore({
  reducer: {
    adoptedPet
  },
});

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch