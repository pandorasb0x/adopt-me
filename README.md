# adopt-me

An app for adopting incredible animals.

## Description

Project for [Complete Intro to React V8](https://react-v8.holt.courses)

## Technical description + Tech Stack

We firstly create a React application just with pure react, and not with
npx create-react-app adopt-me
(we added the scripts for react and react dom in the index.html file).

Then, we removed those scripts.
We installed npm to be able to manage our dependencies.
As a code formmatter we choose Prettier and ESlint for detecting any error:

`npm run lint -- --debug`

As a build tool we are using Vite.

And finally we added react dependency using npm.

## Run the application

It requires node version > 17

`npm install`

`npm run dev`

## Advise

- Dont do this because is very implicit: `<Pet {...pet}/>`

Instead, make the contractor explicit:

`<Pet animal={pet.animal} key={pet.id} name={pet.name} breed={pet.breed} images={pet.images} />`

Where is appropriate? If you have a component that is meant to be a total pass through component, which is not meant to do anything: all its meant to do is take in something and then apply some effect and pass it onward, and it doestnt actually need to know anything about that.

- Minimize effects in your code: to write long term maintainable React code because the hardest part about React is useEffect.

- In this code, fetchBreedList and fetchPet, are quite similar. However is not recommended to make an abstraction because abstractions are not friends of maintenability.

- Optional chaining operator (Sif something in the chain fails, i will get an empty array):

`results?.data?.breeds ?? []`

- Use a library for cache when fetching data, like useQuery from tanstack

- We have used a controlled form (controlled by react with useState for each input). But, since we just need the inputs for API calls, it would be awesome not to use useState. We could just let the browser take care of it and then pull the data out of the browser whenever we had a sybmit event.

- Choosing between class components or functional components is only about taste.
  useEffect kind of mixes all the lifecycle. There's some superfine tuning you can do with performance with a class component that is maybe more difficult to accomplish: for example there is a thing called ShouldComponentUpdate().
  If you want to call an error logging service(Sentry or TrackJs p.e.), componentDidCatch would be an amazing place to do that, therefore you'll need to use classes.
- When you do an arrow function it does not creat new scope (always will refer to the component) versus when you have a normal function whenever you invoke it it creates a new scope at the point of invocation.

- Repeat yourself a couple of times until you abstract. Because if not, youre adding complexity maybe for just 1 use case. So repeat yourself twice and in the third case start thinking of abstracting.

- To manage modals, or navigation stuff (that you need to control in one component but the thing needs to be rendered somewhere else) is recommended to use Portals.

- If you have a lot of pages taking care for the same thing (lets say login, theme, etc.) then is recommended to use Context.

- For specific little things use Context, but if you are handling all the data of the App, instead of making a giant snowball (or having multiple contexts), is recommended to use Redux.

---

## HOOKS

This is the link to [Stackblitz](https://stackblitz.com/edit/ir5?file=src%2Froutes%2FUseRef.jsx&hideExplorer=1&initialPath=/useRef&view=both)

Hooks are functions that let you “hook into” React state and lifecycle features from function components. Hooks don't work inside classes — they let you use React without classes. (We don't recommend rewriting your existing components overnight but you can start using Hooks in the new ones if you'd like.)

**UseRef**: Allows you to have a container so that you can have the same value beetween renders. The usecase for a ref is to keep track of the exact same thing.

**UseReducer**: A reducer is just a fancy way of saying I have a function that takes a bit of state. It takes an action and it gives you back a new bit of state.

**UseMemo**: You give it a function to recalculate something and you tell it when to recalculate. Is used for very expensive calculations, when you start seeing jank is a good opportunity for using this hook.

**useCallback**: is quite similar and indeed it's implemented with the same mechanisms as useMemo.

**useLayoutEffect**: is almost the same as useEffect except that it's synchronous to render as opposed to scheduled like useEffect is. If you're migrating from a class component to a hooks-using function component, this can be helpful too because useLayoutEffect runs at the same time as componentDidMount and componentDidUpdate whereas useEffect is scheduled after. This should be a temporary fix. The only time you should be using useLayoutEffect is to measure DOM nodes for things like animations.

**useId**: frequently in React you need unique identifiers to associate two objects together. An example of this would be making sure a label and an input are associated together by the htmlFor attribute. If you need multiple IDs in the same component just do {id}-name, {id}-address, ``{id}-number, etc. No need to call useId` multiple times.

## Advise

- Dont wrap everything in useMemo, bc this introduce a new problem: why isnt this rerendering?. Use it only when you need it.

- Do not do performance optimizations until you have a performance problem. Have a problem before u solve the problem.

- Some people says useReducer is a replacement for useState. useState and useReducer accomplish the same thing, just different ways. If I have a sort of complex interaction between of how my state is changing over time, useReducer is a elegant way to solve that.
  But if I'm just going from true to false and false to true, just use useState

## Code Splitting

Code splitting is essential to having small application sizes, particularly with React. React is already forty-ish kilobytes just for the framework. This isn't huge but it's enough that it will slow down your initial page loads (by up to a second and a half on 2G speeds.) If you have a lot third party libraries on top of that, you've sunk yourself before they've even started loading your page.

Enter code splitting. This allows us to identify spots where our code could be split and let Vite do its magic in splitting things out to be loaded later. An easy place to do this would be at the route level.

//instead of:
`import { Modal } from "./Modal";`

// do the following (lazy will call the function once it is trying to be rendered):
`import { useContext, useState, lazy } from "react";`

` Modal = lazy(() => import("./Modal"));`

**To do code splitting, you need to be splitting tens of kilobytes to make sense**.

If you want to load thing in the background you can but you need to use: service worker, and youll need to have a manifest in your Webpack, parce, etc., with all the chunks that need to be loaded by the service worker.

## Server Side Rendering

Performance is a central concern for front end developers. We should always be striving to serve the leanest web apps that perform faster than humans can think. It's a challenge of loading the correct content first so a user can see a site and begin to make a decision of what they want to do (scroll down, click a button, log in, etc.) and then be prepared for that action before they make that decision.

Enter server-side rendering. This is a technique where you run React on your Node.js server before you serve the request to the user and send down the first rendering of your website already done. This saves precious milliseconds+ on your site because otherwise the user has to download the HTML, then download the JavaScript, then execute the JS to get the app. In this case, they'll just download the HTML and see the first rendered page while React is loading in the background.

While the total time to when the page is actually interactive is comparable, if a bit slower, the time to when the user sees something for the first time should be much faster, hence why this is a popular technique. So let's give it a shot.

## useDeferredValue

This hook is for low priority re-rendering. If a user clicks on something, Facebook dont care if you are loading more stuff below the fold, they just care to re-render things that user is focusing on.

Only use it if you're seeing problems of you having something that's expensive to render, that could be delayed, and your users are perceiving jank because they are trying to do something else instead of perceiving whatever it is we rendering there.

## useTransition

You can show an intermediare state, and then if it needs to be interrupted, its also interruptible. Its both: low priority and help us show a good loading state in these intermediary periods.
